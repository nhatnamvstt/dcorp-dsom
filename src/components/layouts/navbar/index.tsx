import { Menu } from "antd";
import routePaths from "../../../routes/route-paths";
import { useNavigate } from "react-router-dom";

const Navbar = () => {
    const navigate = useNavigate();

    const menus = [
        {
            key: 1,
            icon: null,
            label: 'Master data',
            type: 'group',
            children: [
                {
                    key: 1_1,
                    href: routePaths.products,
                    label: 'Danh sách hàng hóa',
                },

                {
                    key: 1_2,
                    href: routePaths.productCategories,
                    label: 'Danh sách nhóm hàng',
                },

                {
                    key: 1_3,
                    href: routePaths.partners,
                    label: 'Danh sách công ty',
                },

                {
                    key: 1_4,
                    href: routePaths.restaurants,
                    label: 'Danh sách nhà hàng',
                }
            ],
        },

        {
            key: 2,
            icon: null,
            label: 'Bán hàng',
            type: 'group',
            children: [
                {
                    key: 2_1,
                    href: routePaths.salesOrders,
                    label: 'Danh sách bán hàng',
                },
            ],
        },

        {
            key: 3,
            icon: null,
            label: 'Báo cáo',
            type: 'group',
            children: [
                {
                    key: 3_1,
                    href: routePaths.salesOrderReportByDate,
                    label: 'Bán hàng theo ngày',
                },
            ],
        }
    ]

    const onClick = (event: any) => {
        const href = event.item.props.href || '';

        navigate(href)
    }

    return (
        <Menu
            onClick={onClick}
            items={menus}
        />
    )
};

export default Navbar;
import { Layout } from "antd";
import { FC } from "react";
import { Link, Outlet } from "react-router-dom";
import Navbar from "./navbar";
import routePaths from "../../routes/route-paths";
import Title from "antd/es/typography/Title";
import Text from "antd/es/typography/Text";

const LayoutPage: FC = () => {
    return (
        <Layout hasSider>
            <Layout.Sider
                theme="light"
                breakpoint="md"
                collapsedWidth="0"
                onBreakpoint={(broken) => {
                    console.log(broken);
                }}
                onCollapse={(collapsed, type) => {
                    console.log(collapsed, type);
                }}
                style={{ overflowY: 'auto', height: '100vh', position: 'fixed', left: 0, top: 0, bottom: 0 }}
            >
                <Link to={routePaths.welcome}>
                    <div style={{ textAlign: "center", padding: '24px' }}>
                        <Title className="text-primary my-0">DSOM</Title>
                        <Text>Dcorp Sale order management</Text>
                    </div>
                    <div className="demo-logo-vertical" />
                </Link>
                <Navbar />
            </Layout.Sider>

            <Layout style={{ marginLeft: 200 }}>
                <Layout.Content style={{ padding: '24px 16px 0', minHeight: "calc(100vh)", overflowY: 'initial', overflowX: 'hidden'}}>
                    <Outlet />
                </Layout.Content>
            </Layout>
        </Layout>

    )
}

export default LayoutPage;
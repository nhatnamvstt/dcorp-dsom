import { Button, Form, FormProps } from "antd";
import { FC, ReactNode } from "react";

export interface FormContainerProps extends FormProps {
    children?: ReactNode;
    loading?: boolean;
    onCancel?: () => void;
    onSubmit?: (formData: any) => void;
}

const FormContainer: FC<FormContainerProps> = (props) => {
    // ** props
    const { onCancel, onSubmit, children, loading, ...restProps } = props;

    return (
        <Form
            {...restProps} className="h-full w-full flex flex-col"
            onFinish={onSubmit}
        >
            <div className="flex-[2]">
                {children}
            </div>

            <div className="flex items-center justify-between space-x-4">
                <Button className="flex-[2] cursor-pointer" htmlType="button" danger
                    onClick={onCancel}
                >
                    Hủy
                </Button>

                <Button loading={loading} className="flex-[2] cursor-pointer bg-primary" htmlType="submit" type="primary"
                >
                    Lưu
                </Button>
            </div>
        </Form>
    )
};

export default FormContainer;
import { Select } from "antd";
import { FC } from "react";

const TaxSelect: FC = (props) => {
    return (
        <Select
            {...props}
            options={[
                {
                    value: 0,
                    label: '0%',
                },

                {
                    value: 8,
                    label: '8%',
                },

                {
                    value: 10,
                    label: '10%',
                },
            ]}
        />
    )
};

export default TaxSelect;
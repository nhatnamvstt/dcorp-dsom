import { FC } from "react";
import { Select } from "antd";

import { StatusEnum } from "../../../types/common";

const StatusSelect: FC = (props) => {
    return (
        <Select 
            {...props}

            options={[
                {
                    value: StatusEnum.Draft,
                    label: 'Nháp'
                },

                {
                    value: StatusEnum.Deactivate,
                    label: 'Chưa kích hoạt'
                },

                {
                    value: StatusEnum.Active,
                    label: 'Kích hoạt'
                },
            ]}
        />
    )
};

export default StatusSelect;
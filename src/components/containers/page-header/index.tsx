import { Breadcrumb, Card, Col, Row } from "antd";
import { FC, ReactNode } from "react";

import { Typography } from 'antd';

import routePaths from "../../../routes/route-paths";

const { Title } = Typography;

type BreadCrumbType = {
    href?: string;
    title: ReactNode | string
}

interface PageHeaderProps {
    title: string;
    children: ReactNode;
    breadcrumbs?: BreadCrumbType[];
}


const PageHeader: FC<PageHeaderProps> = (props) => {
    // ** props
    const { title = "", breadcrumbs = [], children } = props;

    return (
        <div className="flex flex-col h-full space-y-1">
            <div className="flex-[0_1_fit-content] m-h-fit">
                <Row>
                    <Col span={24}>
                        <Title level={3} className="m-0">{title}</Title>
                    </Col>

                    <Col span={24}>
                        <Breadcrumb
                            items={[
                                {
                                    href: routePaths.welcome,
                                    title: 'Trang chủ'
                                },

                                ...breadcrumbs,
                            ]}
                        />
                    </Col>
                </Row>
            </div>

            <div className="flex-[2]">
                <Card className="h-full">
                    {children}
                </Card>
            </div>
        </div>
    )
}

export default PageHeader;
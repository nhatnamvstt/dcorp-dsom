import axios, { AxiosError, AxiosInstance, AxiosResponse } from "axios";

const handleResponseSuccess = (response: AxiosResponse ) => {
    console.log(response);
    return response.data;
}

const handleResponseFailure = (error: AxiosError) => {
    console.log(error);
    return Promise.reject(error);
};

const HttpInstance = (): AxiosInstance => {
    const http = axios.create({
        baseURL: 'http://118.69.108.41:5003/api/',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }

    });
    
    http.interceptors.response.use(handleResponseSuccess, handleResponseFailure);

    return http;
}

const http = HttpInstance();

export { http };

export default HttpInstance();
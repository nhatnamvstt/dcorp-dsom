import { http } from "..";

export const reportApis = {
    getReportByDate: (params: any) => {
        return http.get('/orders/statistical', { params });
    },
}
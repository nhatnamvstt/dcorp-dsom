import { http } from "..";

export const saleOrderApis = {
    getSaleOrders: (params: any) => {
        return http.get('/orders', { params });
    },

    createSaleOrder: (payload: any) => {
        return http.post('/orders', payload);
    },

    updateSaleOrder: (payload: any) => {
        return http.put('/orders', payload);
    },

    deleteSaleOrder: (saleOrderId: number) => {
        return http.delete('/orders', { params: {id: saleOrderId} });
    },
}
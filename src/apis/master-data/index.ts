import { http } from ".."
import { WITHOUT_LIMIT } from "../../constant";

export const productCategoryApis = {
    getProductCategory: (params: any) => {
        return http.get('/productcategories', { params });
    },

    getProductCategoryWP: () => {
        return http.get('/productcategories', { params: { pageIndex: 1, pageSize: WITHOUT_LIMIT }});
    },
}

export const companyApis = {
    getCompany: (params: any) => {
        return http.get('/companies', { params });
    },

    getCompanyWP: () => {
        return http.get('/companies', { params: { pageIndex: 1, pageSize: WITHOUT_LIMIT }});
    },
}

export const storeApis = {
    getStores: (params: any) => {
        return http.get('/stores', { params });
    },

    getStoreWP: () => {
        return http.get('/stores', { params: { pageIndex: 1, pageSize: WITHOUT_LIMIT }});
    },
}

export const productApis = {
    createProduct: (payload: any) => {
        return http.post('/products', payload);
    },

    updateProduct: (payload: any) => {
        return http.put('/products', payload);
    },

    getProducts: (params: any) => {
        return http.get('/products', {params});
    },

    getProductWP: (params: any) => {
        return http.get('/products', { params: { pageIndex: 1, pageSize: WITHOUT_LIMIT }});
    },

    deleteProduct: (id: number) => {
        return http.delete('/products', { params: { id: id }});
    }
}
import { useEffect, useMemo, useState } from "react";

import Search from "antd/es/input/Search";
import Text from "antd/es/typography/Text";
import { Col, Row, Select, Table, TableProps } from "antd";

import PageHeader from "../../../components/containers/page-header";

import usePagination from "../../../utils/hooks/use-pagination";
import { companyApis, storeApis } from "../../../apis/master-data";
import { DEFAULT_LIMIT } from "../../../constant";
import { toast } from "react-toastify";

const RestaurantPage = () => {
    // ** hooks
    const { total, setTotal, pagination, page } = usePagination();

    // ** statement
    const [companyId, setCompanyId] = useState(0);
    const [search, setSearch] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [restaurants, setRestaurants] = useState<any[]>([]);
    const [companies, setCompanies] = useState<any[]>([]);

    const columns: TableProps<any>['columns'] = [
        {
            title: 'Stt',
            key: 'no',
            dataIndex: 'no',
            className: 'text-center',
            render: (text: number, record: any, index: number) => index + (page - 1) * DEFAULT_LIMIT + 1
        },

        {
            title: 'Tên nhà hàng',
            key: 'name',
            width: 200,
            dataIndex: 'name',
        },

        {
            title: 'Tên công ty',
            key: 'companyName',
            width: 200,
            dataIndex: 'companyName',
            className: 'whitespace-nowrap',
            render: (companyId: number) => {
                const company = companies.find((item: any) => item.id === companyId);

                if (company) {
                    return company.name
                }

                return null
            }
        },

        {
            title: 'Địa chỉ',
            key: 'address',
            width: 350,
            dataIndex: 'address',
        },
    ]

    const companyOptions = useMemo(() => {
        if (!companies) { return [] };

        return companies.map((company: any) => ({
            value: company.id,
            label: company.name
        }))
    }, [companies])

    const loadRestaurants = async () => {
        try {
            setLoading(true);
            const response = await storeApis.getStores({
                search,
                companyId,
                pageIndex: page,
                pageSize: DEFAULT_LIMIT,
            });

            setTotal(response?.data?.total || 0);
            setRestaurants(response?.data?.records || []);

        } catch (error: any) {
            toast.error(error?.message)
        } finally {
            setLoading(false);
        }
    }

    const loadCompanies = async () => {
        try {
            const response = await companyApis.getCompanyWP();

            setCompanies(response?.data?.records || []);

        } catch (error: any) {
            console.log(error);
        }
    }

    useEffect(() => {
        loadRestaurants();
    }, [page, search, companyId]);

    useEffect(() => {
        loadCompanies();
    }, [])
    
    return (
        <PageHeader
            title="Danh sách nhà hàng"
            breadcrumbs={[{ title: "Danh sách nhà hàng" }]}
        >
            <Row className="space-y-2">
                <Col span={24}>
                    <Row align={'bottom'} justify={'space-between'}>
                        <span>
                            Tổng nhà hàng: {total}
                        </span>

                        <Row gutter={[12, 12]}>
                            <Col className="relative">
                                <Text className="absolute -top-5 left-2 !z-[999] bg-white p-1 pt-2.5 pb-0 text-[10px]">Công ty</Text>

                                <Select
                                    options={[
                                        {
                                            value: 0,
                                            label: 'Tất cả'
                                        },

                                        ...companyOptions
                                    ]}
                                    value={companyId}
                                    className="w-[150px] relative z-10"
                                    onChange={(companyId: number) => setCompanyId(companyId)}
                                />
                            </Col>

                            <Col>
                                <Search className="w-[220px]" placeholder="Tìm theo tên nhà hàng" 
                                    onSearch={(text: string) => {
                                        setSearch(text);
                                    }}
                                />
                            </Col>
                        </Row>
                    </Row>
                </Col>

                <Col span={24}>
                    <Table
                        rowKey={'id'}
                        loading={loading}
                        columns={columns}
                        dataSource={restaurants}
                        pagination={pagination}
                        locale={{ emptyText: 'Không có dữ liệu nhà hàng' }}
                    />
                </Col>
            </Row>
        </PageHeader>
    )
}

export default RestaurantPage;    
import { useEffect, useState } from "react";

import Search from "antd/es/input/Search";
import { Col, Row, Table, TableProps } from "antd";

import PageHeader from "../../../components/containers/page-header";

import { formatTimeToString } from "../../../utils/time";
import usePagination from "../../../utils/hooks/use-pagination";
import { companyApis } from "../../../apis/master-data";
import { DEFAULT_LIMIT } from "../../../constant";
import { toast } from "react-toastify";
import { StatusEnum, StatusLabelEnum } from "../../../types/common";

const PartnerPage = () => {
    // ** hooks
    const { total, setTotal, pagination, page } = usePagination();

    // ** statement
    const [search, setSearch] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [companies, setCompanies] = useState<any[]>([]);

    const columns: TableProps<any>['columns'] = [
        {
            title: 'Stt',
            key: 'no',
            dataIndex: 'no',
            className: 'text-center',
            render: (text: number, record: any, index: number) => index + (page - 1) * DEFAULT_LIMIT + 1
        },

        {
            title: 'Tên công ty',
            key: 'name',
            width: 200,
            dataIndex: 'name',
        },

        {
            title: 'Mô tả',
            key: 'description',
            dataIndex: 'description',
            width: 300,
            render: (text: string) => text?.length > 20 ? text.substring(0, 20) + '...' : text
        },

        {
            title: 'Trạng thái',
            key: 'status',
            dataIndex: 'status',
            className: 'whitespace-nowrap text-center',
            render: (status: number) => StatusLabelEnum[status as StatusEnum]
        },
    ]

    const loadCompanies = async () => {
        try {
            setLoading(true);
            const response = await companyApis.getCompany({
                search,
                pageIndex: page,
                pageSize: DEFAULT_LIMIT,
            });

            setTotal(response?.data?.total || 0);
            setCompanies(response?.data?.records || []);
        } catch(error: any) {
            toast.error(error?.message)
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        loadCompanies();
    }, [page, search]);

    return (
        <PageHeader
            title="Danh sách công ty"
            breadcrumbs={[{ title: "Danh sách công ty" }]}
        >
            <Row className="space-y-2">
                <Col span={24}>
                    <Row align={'bottom'} justify={'space-between'}>
                        <span>
                            Tổng công ty: {total}
                        </span>

                        <Search className="w-[300px]" placeholder="Tìm theo tên công ty / mã số thuế" 
                            onSearch={(text: string) => {
                                setSearch(text);
                            }}
                        />
                    </Row>
                </Col>

                <Col span={24}>
                    <Table
                        rowKey={'id'}
                        loading={loading}
                        columns={columns}
                        dataSource={companies}
                        pagination={pagination}
                        locale={{ emptyText: 'Không có dữ liệu công ty' }}
                    />
                </Col>
            </Row>
        </PageHeader>
    )
}

export default PartnerPage;    
import { FC, ReactNode, createContext, useContext, useEffect, useMemo, useState } from "react";
import { IProductCategory } from "../../../../types/master-data/product-categories";

// ** Data
import ProductCategories from '../../../../@fake-data/master-data/product-categories.json';
import { OptionType } from "../../../../types/common";
import { productCategoryApis } from "../../../../apis/master-data";

interface CategoryContextType {
    categories: IProductCategory[],
    categoryOptions: OptionType<number>[],
}

const DEFAULT_CONTEXT: CategoryContextType = {
    categories: [],
    categoryOptions: [],
}

const Context = createContext<CategoryContextType>(DEFAULT_CONTEXT);

interface ProductCategoriesProviderProps {
    children: ReactNode
}

const ProductCategoriesProvider: FC<ProductCategoriesProviderProps> = (props) => {
    const [categories, setCategories] = useState<IProductCategory[]>([]);

    const categoryOptions = useMemo(() => {
        return categories.map((item: IProductCategory) => ({
            value: item.id,
            label: item.name,
        }))
    }, [categories]);

    const loadProductCategories = async () => {
        try {
            const response = await productCategoryApis.getProductCategoryWP()

            setCategories(response?.data?.records);
            
        } catch(error: any) {
            console.log(error?.message || '');
        }
    }

    useEffect(() => {
        if (!categories.length) {
            loadProductCategories()
        }
    }, [categories]);

    return (
        <Context.Provider
            value={{
                categories,
                categoryOptions,
            }}
        >
            {props.children}
        </Context.Provider>
    )
}

export const useProductCategoryContext = (): CategoryContextType => useContext(Context);

export default ProductCategoriesProvider;

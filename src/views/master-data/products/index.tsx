import { useEffect, useState } from "react";

import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Button, Col, Drawer, Modal, Row, Table, TableProps } from "antd";

import PageHeader from "../../../components/containers/page-header";

import usePagination from "../../../utils/hooks/use-pagination";

import ProductCategoriesProvider from "./context";

import ProductFilter from "./components/filter";
import SelectCategories from "./components/prev-select-categories";

import { ProductActionPayloadType, ProductType } from "../../../types/master-data/products";
import Text from "antd/es/typography/Text";

// ** Data
import { formatNumberToAmount, formatNumberToPercent } from "../../../utils/number";
import ProductFormActions from "./components/form-action";
import { StatusEnum } from "../../../types/common";
import { ConfirmAlert } from "../../../utils/confirm-alert";
import { toast } from "react-toastify";
import { productApis } from "../../../apis/master-data";
import { DEFAULT_LIMIT } from "../../../constant";

const ProductPage = () => {
    // ** hooks
    const { total, setTotal, pagination, page, setPage } = usePagination();

    // ** statement
    const [loading, setLoading] = useState<boolean>(false);
    const [actionLoading, setActionLoading] = useState<boolean>(false);
    const [products, setProducts] = useState<ProductType[]>([]);
    const [categoryId, setCategoryId] = useState<number | null>(null);
    const [isPrevCategorySelector, setIsPrevCategorySelector] = useState<boolean>(false);

    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [isActionOpen, setIsActionOpen] = useState<boolean>(false);
    const [productAction, setProductAction] = useState<ProductType | null>(null);

    const handleProductEdit = async (product: ProductType) => {
        setIsEdit(true);
        setIsActionOpen(true);
        setProductAction(product);
    }

    const columns: TableProps<ProductType>['columns'] = [
        {
            title: 'Stt',
            key: 'no',
            dataIndex: 'no',
            className: 'text-center',
            render: (text: number, record: any, index: number) => index + (page - 1) * DEFAULT_LIMIT + 1
        },

        {
            title: 'Mã',
            key: 'code',
            dataIndex: 'code',
        },

        {
            title: 'Tên',
            key: 'name',
            dataIndex: 'name',
        },

        {
            title: 'Giá',
            key: 'price',
            dataIndex: 'price',
            className: 'text-right',
            render: (amount: number) => formatNumberToAmount(amount)
        },

        {
            title: 'Thuế',
            key: 'tax',
            dataIndex: 'tax',
            className: 'text-right',
            render: (amount: number) => formatNumberToPercent(amount)
        },

        {
            title: 'Mô tả',
            key: 'description',
            dataIndex: 'description',
            render: (text: string) => text.length > 20 ? text.substring(0, 20) + '...' : text
        },

        {
            title: '',
            key: 'action',
            dataIndex: 'action',
            className: 'text-right',
            render: (text: any, record: ProductType) => (
                <div className="flex items-center space-x-2 justify-end">
                    <div className="border rounded p-1 cursor-pointer" onClick={() => handleProductEdit(record)}>
                        <EditOutlined />
                    </div>

                    <div className="border rounded p-1 cursor-pointer" onClick={() => handleProductDelete(record.id ?? 0)}>
                        <DeleteOutlined />
                    </div>
                </div>
            )
        },
    ]

    const handleSelectModalOk = (categoryId: any) => {
        setCategoryId(categoryId);
        setIsPrevCategorySelector(false);
    };

    const loadProducts = async () => {
        try {
            setLoading(true);
            const response = await productApis.getProducts({
                pageIndex: page,
                pageSize: DEFAULT_LIMIT,
                productCategoryId: categoryId,
            });

            setTotal(response?.data?.total);
            setProducts(response?.data?.records);

        } catch (error: any) {
            toast.error(error.message);
        } finally {
            setLoading(false);
        }
    };

    // ** form action
    const openProductAction = () => {
        setIsActionOpen(true);
    };

    const closeProductAction = () => {
        setIsEdit(false);
        setProductAction(null);
        setIsActionOpen(false);
    };

    const createProduct = async (payload: ProductActionPayloadType) => {
        try {
            setActionLoading(true);

            const response: any = await productApis.createProduct(payload);

            if (!response?.success) {
                throw Error(response?.message || '');
            }

            if (page === 1) {
                loadProducts();
            } else {
                setPage(1)
            }

            closeProductAction();

        } catch(error: any) {
            toast.error(error?.message);
        } finally {
            setActionLoading(false);
        }
    }

    const updateProduct = async (payload: any) => {
        try {
            setActionLoading(true);

            const updatePayload = {
                id: productAction?.id,
                ...payload,
            }
            const response: any = await productApis.updateProduct(updatePayload);

            if (!response?.success) {
                throw Error(response?.message || '');
            }

            loadProducts();
            closeProductAction();

        } catch(error: any) {
            toast.error(error?.message);
        } finally {
            setActionLoading(false);
        }
    }

    const deleteProduct = async (productId: number) => {
        try {
            const response: any = await productApis.deleteProduct(productId);
            if (!response?.success) {
                throw Error(response?.message || '');
            }

            loadProducts();

        } catch(error: any) {
            toast.error(error.message);
        }
    }

    const handleFormSubmit = async (productData: ProductActionPayloadType) => {
        if (!isEdit) {
            createProduct(productData);
            return;
        }

        updateProduct(productData);
    }

    const handleProductDelete = async (productId: number) => {
        ConfirmAlert({
            content: <Text>Bạn có muốn xóa item này</Text>,
            onOk: () => deleteProduct(productId),
        });
    }

    useEffect(() => {
        if (categoryId) {
            loadProducts();
        }
    }, [categoryId])

    useEffect(() => {
        if (!categoryId) {
            setIsPrevCategorySelector(true);
        }
    }, [categoryId]);

    return (
        <PageHeader
            title="Danh sách hàng hóa"
            breadcrumbs={[{ title: "Danh sách hàng hóa" }]}
        >
            <ProductCategoriesProvider>
                <>
                    <Row className="space-y-2">
                        <Col span={24}>
                            <Row align={'bottom'} justify={'space-between'}>
                                <span>
                                    Tổng lượng hàng hóa: {total}
                                </span>
                                <Row gutter={[12, 12]}>
                                    <Col>
                                        <ProductFilter filter={{ categoryId }} onFilterChange={(value) => setCategoryId(value.categoryId)} />
                                    </Col>

                                    <Button type="primary" className="bg-primary" onClick={openProductAction}>
                                        Thêm
                                    </Button>
                                </Row>
                            </Row>
                        </Col>

                        <Col span={24}>
                            <Table
                                rowKey={'id'}
                                columns={columns}
                                dataSource={products}
                                pagination={pagination}
                                locale={{ emptyText: 'Không có dữ liệu nhóm hàng hóa' }}
                            />
                        </Col>
                    </Row>

                    {isPrevCategorySelector && (
                        <SelectCategories
                            open={isPrevCategorySelector}
                            onSelectCategory={() => null}

                            onOk={handleSelectModalOk}
                            onCancel={() => setIsPrevCategorySelector(false)}
                        />
                    )}

                    {isActionOpen && (
                        <Drawer
                            title={isEdit ? 'Sửa thông tin hàng': 'Thêm hàng mới'}
                            open={isActionOpen}
                            onClose={closeProductAction}
                        >
                            <ProductFormActions
                                loading={actionLoading}
                                initial={productAction ? { ...productAction, productCategoryId: categoryId as number } : { productCategoryId: categoryId as number, status: StatusEnum.Draft }}
                                onCancel={closeProductAction}
                                onSubmit={handleFormSubmit}
                            />
                        </Drawer>
                    )}
                </>
            </ProductCategoriesProvider>
        </PageHeader>
    )
}

export default ProductPage;    
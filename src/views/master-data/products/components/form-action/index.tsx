import { Col, Drawer, DrawerProps, Form, Input, InputNumber, Row, Select } from "antd";
import { FC } from "react";
import FormContainer, { FormContainerProps } from "../../../../../components/utils/form-container";
import { useProductCategoryContext } from "../../context";
import { formatAmountToNumber, formatNumberToAmount } from "../../../../../utils/number";
import TaxSelect from "../../../../../components/utils/tax-select";
import StatusSelect from "../../../../../components/utils/status-select";
import { StatusEnum } from "../../../../../types/common";
import { ProductActionPayloadType, ProductType } from "../../../../../types/master-data/products";

interface ProductFormActionProps extends FormContainerProps {
    initial?: ProductType | null,
    loading?: boolean
}

const ProductFormActions: FC<ProductFormActionProps> = (props) => {
    // ** context
    const { categoryOptions } = useProductCategoryContext();

    // ** props
    const { initial = { status: StatusEnum.Draft, }, loading } = props;

    // ** form
    const [form] = Form.useForm<ProductActionPayloadType>();

    return (
        <FormContainer
            {...props}
            form={form}
            layout="vertical"
            loading={loading}
            disabled={loading}
            initialValues={{
                ...initial,
            }}
        >
            <Form.Item
                name="productCategoryId"
                label="Nhóm hàng"
                required
                className="mb-2"

                rules={[
                    {
                        required: true,
                        message: ''
                    }
                ]}
            >
                <Select
                    options={categoryOptions}
                    className="w-full"
                />
            </Form.Item>

            <Form.Item
                name="code"
                label="Mã hàng"
                required
                className="mb-2"

                rules={[
                    {
                        required: true,
                        message: ''
                    }
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name="name"
                label="Tên"
                required
                className="mb-2"

                rules={[
                    {
                        required: true,
                        message: ''
                    }
                ]}
            >
                <Input />
            </Form.Item>

            <Row gutter={[12, 12]}>
                <Col span={12}>
                    <Form.Item
                        name="price"
                        label="Giá"
                        required
                        className="mb-2"

                        rules={[
                            {
                                required: true,
                                message: ''
                            }
                        ]}
                    >
                        <InputNumber
                            className="w-full"
                            formatter={(value: number | undefined) => formatNumberToAmount(value as number)}
                            parser={(value: string | undefined) => formatAmountToNumber(value as string)}
                        />
                    </Form.Item>
                </Col>

                <Col span={12}>
                    <Form.Item
                        name="tax"
                        label="Thuế"
                        required
                        className="mb-2"

                        rules={[
                            {
                                required: true,
                                message: ''
                            }
                        ]}
                    >
                        <TaxSelect />
                    </Form.Item>
                </Col>
            </Row>

            <Form.Item
                name="description"
                label="Mô tả"
                required
                className="mb-2"

                rules={[
                    {
                        required: true,
                        message: ''
                    }
                ]}
            >
                <Input.TextArea rows={4} />
            </Form.Item>

            <Form.Item
                name="status"
                label="Trạng thái"
                required
                className="mb-2"

                rules={[
                    {
                        required: true,
                        message: ''
                    }
                ]}

            >
                <StatusSelect />
            </Form.Item>
        </FormContainer>
    )
};

export default ProductFormActions;
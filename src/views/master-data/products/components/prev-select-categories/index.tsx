import { Col, Modal, ModalProps, Row, Select } from "antd";
import { FC, useState } from "react";

import Text from "antd/es/typography/Text";
import { useProductCategoryContext } from "../../context";

interface SelectCategorySelectProps extends ModalProps {
    onSelectCategory: (categoryId: number) => void;
}

const SelectCategories: FC<SelectCategorySelectProps> = ({ onSelectCategory, ...props }) => {
    // ** Context
    const { categoryOptions } = useProductCategoryContext();
    
    // ** Statement
    const [selected, setSelected] = useState<number | null>(null)
    
    return (
        <Modal
            {...props}
            width="400px"
            onOk={() => props.onOk?.(selected as any)}
            okButtonProps={{ type: 'primary', className: 'bg-primary' }}
        >
            <Row>
                <Col span={24}>
                    <Text>Chọn loại hàng hóa</Text>
                </Col>

                <Col span={24}>
                    <Select
                        options={categoryOptions}
                        className="w-full"
                        onChange={(value) => setSelected(value)}
                    />
                </Col>
            </Row>

        </Modal>
    )
};

export default SelectCategories;
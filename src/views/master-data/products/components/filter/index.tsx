import { Col, Row, Select } from "antd";
import { FC } from "react";

import Search from "antd/es/input/Search";
import Text from "antd/es/typography/Text";
import { useProductCategoryContext } from "../../context";

type FilterType = {
    categoryId: number | null,
}

interface ProductFilterProps {
    filter?: FilterType

    onSearch?: (value: string) => void
    onFilterChange?: (value: FilterType) => void
}

const ProductFilter: FC<ProductFilterProps> = ({ filter, onSearch, onFilterChange, ...props }) => {
    // ** context
    const { categoryOptions } = useProductCategoryContext();

    return (
        <div className="relative">
            <Text className="absolute -top-5 left-2 !z-[999] bg-white p-1 pt-2.5 pb-0 text-[10px]">Nhóm hàng</Text>

            <Select
                value={filter?.categoryId ?? 0}
                options={categoryOptions}
                className="w-[150px] relative z-10"

                onChange={(value) => onFilterChange?.({ categoryId: value })}
            />

        </div>
    )
};

export default ProductFilter;
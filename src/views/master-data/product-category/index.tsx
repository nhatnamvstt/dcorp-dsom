import { useEffect, useState } from "react";

import Search from "antd/es/input/Search";
import { Col, Row, Table, TableProps } from "antd";

import PageHeader from "../../../components/containers/page-header";

import { formatTimeToString } from "../../../utils/time";
import usePagination from "../../../utils/hooks/use-pagination";

import { IProductCategoriesResponse, IProductCategory } from "../../../types/master-data/product-categories";

// ** Data
import { productCategoryApis } from "../../../apis/master-data";
import { DEFAULT_LIMIT } from "../../../constant";
import { toast } from "react-toastify";

const ProductCategoryPage = () => {
    // ** hooks
    const { total, setTotal, pagination, page } = usePagination();

    // ** statement
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(false);
    const [expandRowKeys, setExpandRowKey] = useState<number[]>([]);
    const [categories, setCategories] = useState<IProductCategoriesResponse[]>([]);

    const columns: TableProps<any>['columns'] = [
        {
            title: 'Stt',
            key: 'no',
            dataIndex: 'no',
            className: 'text-center',
            render: (text: number, record: IProductCategory, index: number) => index + (page - 1) * DEFAULT_LIMIT + 1
        },

        {
            title: 'Tên nhóm',
            key: 'name',
            width: 500,
            dataIndex: 'name',
        },

        {
            title: 'Ngày tạo',
            key: 'createdDate',
            dataIndex: 'createdDate',
            className: 'whitespace-nowrap',
            render: (date: string) => formatTimeToString(date)
        },
    ]

    const loadProductCategories = async () => {
        try {
            setLoading(true);
            const response = await productCategoryApis.getProductCategory({
                search,
                pageIndex: page,
                pageSize: DEFAULT_LIMIT,
            })

            setTotal(response?.data?.total || 0);
            setCategories(response?.data?.records);
            
        } catch(error: any) {
            toast.error(error?.message || '');
        } finally {
            setLoading(false);
        }
    }

    const expandedRowRender = (record: IProductCategoriesResponse) => {
        return <Table columns={columns} showHeader={false} dataSource={record.children} locale={{ emptyText: 'Không có dữ liệu nhóm hàng hóa' }} pagination={false}/>
    }

    const onTableRowExpand = (expanded: boolean, record: IProductCategory) => {
        const keys = [];

        if (expanded) {
            keys.push(record.id);
        }

        setExpandRowKey(keys);
    };

    useEffect(() => {
        loadProductCategories();
    }, [page, search])

    return (
        <PageHeader
            title="Danh sách nhóm hàng hóa"
            breadcrumbs={[{ title: "Danh sách nhóm hàng hóa" }]}
        >
            <Row className="space-y-2">
                <Col span={24}>
                    <Row align={'bottom'} justify={'space-between'}>
                        <span>
                            Tổng loại hàng hóa: {total}
                        </span>
                        
                        <Search className="w-[250px]" placeholder="Tìm theo nhóm hàng hóa" onSearch={(text: string) => setSearch(text)}/>
                    </Row>
                </Col>

                <Col span={24}>
                    <Table
                        rowKey={'id'}
                        columns={columns}
                        loading={loading}
                        dataSource={categories}
                        pagination={pagination}
                        expandable={{ expandedRowRender, onExpand: onTableRowExpand, expandedRowKeys: expandRowKeys }}
                        locale={{ emptyText: 'Không có dữ liệu nhóm hàng hóa' }}
                    />
                </Col>
            </Row>
        </PageHeader>
    )
}

export default ProductCategoryPage;    
import { useEffect, useMemo, useState } from "react";
import PageHeader from "../../../components/containers/page-header";
import { SaleOrderStatusEnum, SaleOrderStatusString } from "../../../types/sale-orders";
import { formatTimeToString } from "../../../utils/time";
import { Button, Col, DatePicker, Drawer, Modal, Row, Table, TableProps, Tooltip } from "antd";
import usePagination from "../../../utils/hooks/use-pagination";
import { ProductType } from "../../../types/master-data/products";
import { formatNumberToAmount } from "../../../utils/number";
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import ProductCategoriesProvider from "../../master-data/products/context";
import Search from "antd/es/input/Search";
import ItemSelect from "./components/item-select";
import SalesAction from "./components/sales-actions";
import dayjs, { Dayjs } from "dayjs";
import { saleOrderApis } from "../../../apis/sale-order";
import { DATE_FORMAT, DEFAULT_LIMIT } from "../../../constant";
import { toast } from "react-toastify";
import Text from "antd/es/typography/Text";
import { ConfirmAlert } from "../../../utils/confirm-alert";


const SalesOrderHistoryPage = () => {
    // ** hooks
    const { total, setTotal, pagination, page, setPage } = usePagination();

    // ** statement
    const [saleOrders, setSaleOrders] = useState<any[]>([]);
    const [salesAction, setSaleAction] = useState<any | null>(null);
    const [isFormActionOpen, setIsFormActionOpen] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [dateRange, setDateRange] = useState<[Dayjs, Dayjs]>([dayjs().startOf('month'), dayjs()]);
    const [salesOrders, setSalesOrders] = useState<any[]>([]);

    const handleEditSaleOrderClick = (saleOrder: any) => {
        setSaleAction(saleOrder);
        setIsFormActionOpen(true);
    }

    const columns: TableProps<any>['columns'] = [
        {
            title: 'Stt',
            key: 'no',
            dataIndex: 'no',
            className: 'text-center whitespace-nowrap',
            width: 50,
            render: (text: number, record: any, index: number) => index + (page - 1) * DEFAULT_LIMIT + 1,
        },

        {
            title: 'Ngày tạo',
            key: 'orderDate',
            dataIndex: 'orderDate',
            render: (date: string) => formatTimeToString(date),
        },

        {
            title: 'Số đơn',
            key: 'orderNumber',
            dataIndex: 'orderNumber',
        },

        {
            title: 'Khách hàng',
            key: 'companyName',
            dataIndex: 'companyName',
        },

        {
            title: 'Sản phẩm',
            key: 'productName',
            dataIndex: 'items',
            render: (products: any[]) => (
                <div className="flex flex-col space-y-1">
                    {products.map((product: any) => (
                        <Tooltip title={product.productName}>
                            <p className="m-0 whitespace-nowrap truncate ...">{product.productName}</p>
                        </Tooltip>
                    ))}
                </div>
            ),
        },

        {
            title: 'Đơn giá',
            key: 'price',
            dataIndex: 'items',
            render: (products: any[]) => (
                <div className="flex flex-col space-y-1">
                    {products.map((product: any) => (
                        <p className="m-0 whitespace-nowrap">{formatNumberToAmount(product.price)}</p>
                    ))}
                </div>
            ),
        },

        {
            title: 'Tổng tiền',
            key: 'orderTotal',
            className: "text-right",
            dataIndex: 'orderTotal',
            render: (amount: number) => formatNumberToAmount(amount)
        },


        {
            title: 'Trạng thái',
            key: 'orderStatus',
            dataIndex: 'orderStatus',
            render: (status: number) => SaleOrderStatusString[status as SaleOrderStatusEnum],
        },

        {
            title: '',
            key: 'action',
            dataIndex: 'action',
            className: 'text-right w-fit',
            fixed: 'right',
            width: 80,
            render: (text: any, record: ProductType) => (
                <div className="flex items-center space-x-2 justify-end">
                    <div className="border rounded p-1 cursor-pointer" onClick={() => handleEditSaleOrderClick(record)}>
                        <EditOutlined />
                    </div>

                    <div className="border rounded p-1 cursor-pointer" onClick={() => handleProductDelete(record.id ?? 0)}>
                        <DeleteOutlined />
                    </div>
                </div>
            )
        },
    ]

    const dateRangeString = useMemo(() => {
        const startDate = dayjs(dateRange[0]).format(DATE_FORMAT.YMD)
        const endDate = dayjs(dateRange[1]).format(DATE_FORMAT.YMD)

        return {
            startDate,
            endDate,
        }
    }, [dateRange]);

    const closeSaleActionModal = () => {
        setIsFormActionOpen(false);

        setSaleAction(null);
    }

    const loadSaleOrders = async () => {
        try {
            setLoading(true);

            const response: any = await saleOrderApis.getSaleOrders({
                status: 0,
                pageIndex: page,
                pageSize: DEFAULT_LIMIT,
                startDate: dateRangeString.startDate,
                endDate: dateRangeString.endDate,
            });

            if (!response?.success) {
                throw Error(response?.message || '');
            }

            setSalesOrders(response?.data?.records || []);
            setTotal(response?.data?.total || 0)

        } catch (error: any) {
            toast.error(error.message);
        } finally {
            setLoading(false);
        }
    };

    const handleUpdateSaleOrder = () => {
        if (page === 1) {
            loadSaleOrders();
        } else {
            setPage(1);
        }

        closeSaleActionModal();
    }

    const deleteSaleOrder = async (saleOrderId: number) => {
        try {
            const response: any = await saleOrderApis.deleteSaleOrder(saleOrderId);
            if (!response?.success) {
                throw Error(response?.message || '');
            }

            loadSaleOrders();

        } catch(error: any) {
            toast.error(error.message);
        }
    }


    const handleProductDelete = async (productId: number) => {
        ConfirmAlert({
            content: <Text>Bạn có muốn xóa đơn hàng này</Text>,
            onOk: () => deleteSaleOrder(productId),
        });
    }

    useEffect(() => {
        loadSaleOrders();
    }, [dateRange, page])

    return (
        <PageHeader
            title="Danh sách đơn hàng"
            breadcrumbs={[{ title: "Danh sách đơn hàng" }]}
        >
            <ProductCategoriesProvider>
                <>
                    <Row className="space-y-2">
                        <Col span={24}>
                            <Row align={'bottom'} justify={'space-between'}>
                                <span>
                                    Tổng đơn hàng: {total}
                                </span>

                                <Row gutter={[12, 12]}>
                                    <Col>
                                        <DatePicker.RangePicker value={dateRange} onChange={(dateRange: [any, any]) => setDateRange(dateRange)} />
                                    </Col>

                                    <Button type="primary" className="bg-primary" onClick={() => setIsFormActionOpen(true)}>
                                        Thêm
                                    </Button>
                                </Row>
                            </Row>
                        </Col>

                        <Col span={24}>
                            <Table
                                rowKey={'id'}
                                columns={columns}
                                scroll={{ x: 992 }}
                                dataSource={salesOrders}
                                pagination={pagination}
                                locale={{ emptyText: 'Không có dữ liệu đơn hàng' }}
                            />
                        </Col>
                    </Row>

                    <SalesAction
                        open={isFormActionOpen}
                        salesOrder={salesAction}
                        onCancel={closeSaleActionModal}
                        createComplete={handleUpdateSaleOrder}
                    />
                </>
            </ProductCategoriesProvider>
        </PageHeader>
    )
}

export default SalesOrderHistoryPage;    
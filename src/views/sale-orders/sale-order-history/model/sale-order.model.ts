import dayjs from "dayjs"
import { DATE_FORMAT } from "../../../../constant"

export class SaleOrderModel {
    id = 0
    companyId = undefined
    companyName = ""
    storeId = undefined
    storeName = ""
    orderDate = dayjs()
    orderNumber = ""
    orderStatus = 0
    subTotal = 0
    orderTax = 0
    orderDiscount = 0
    status = 3
    items = []

    get totalItemAmount() {
        return this.items.reduce((total: number, item: any) => item.amount + total, 0);
    }

    get discountAmount() {
        return this.orderDiscount * this.totalItemAmount / 100
    }

    get orderTotal() {
        return this.totalItemAmount - this.discountAmount
    }

    get formValue() {
        return {
            status: this.status,
            items: this.items,
            storeId: this.storeId,
            subTotal: this.subTotal,
            companyId: this.companyId,
            orderStatus: this.orderStatus,
            orderDiscount: this.orderDiscount,
            orderDate: this.orderDate,
            orderTotal: this.orderTotal,
        }
    }

    get createPayload() {
        return {
            companyId: this.companyId,
            storeId: this.storeId,
            orderDate: dayjs(this.orderDate).format(DATE_FORMAT.YMD),
            orderStatus: this.orderStatus,
            subTotal: this.orderTotal,
            orderTax: this.orderTax,
            orderDiscount: this.orderDiscount,
            orderTotal: this.orderTotal,
            status: 3,
            items: this.items,
            
        }
    }

    get updatePayload() {
        return {
            id: this.id,
            companyId: this.companyId,
            storeId: this.storeId,
            orderDate: dayjs(this.orderDate).format(DATE_FORMAT.YMD),
            orderStatus: this.orderStatus,
            subTotal: this.orderTotal,
            orderTax: this.orderTax,
            orderDiscount: this.orderDiscount,
            orderTotal: this.orderTotal,
            status: this.status,
            items: this.items,
            
        }
    }

    mapFromData(data: any) {
        this.id = data?.id || 0
        this.companyId = data?.companyId || undefined
        this.storeId = data?.storeId || undefined
        this.orderDate = data?.orderDate ? dayjs(data?.orderDate) : dayjs()
        this.orderNumber = data?.orderNumber || ''
        this.orderStatus = data?.orderStatus || 0
        this.subTotal = data?.subTotal || 0
        this.orderTax = data?.orderTax || 0
        this.orderDiscount = data?.orderDiscount || 0
        this.status = data?.status || 0
        this.items = data?.items || []
    }

    assign(data: any) {
        this.id = data?.id || 0
        this.companyId = data?.companyId || undefined
        this.storeId = data?.storeId || undefined
        this.orderDate = data?.orderDate || dayjs()
        this.orderNumber = data?.orderNumber || ''
        this.orderStatus = data?.orderStatus || 0
        this.subTotal = data?.subTotal || 0
        this.orderTax = data?.orderTax || 0
        this.orderDiscount = data?.orderDiscount || 0
        this.status = data?.status || 0
        this.items = data?.items || []
    }

    static build(data?: any) {
        const saleOrderModel = new SaleOrderModel();

        saleOrderModel.mapFromData(data);

        return saleOrderModel
    }
}
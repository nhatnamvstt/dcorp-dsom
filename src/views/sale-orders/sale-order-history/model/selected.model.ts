export class ItemSelectModel {
    id = 0
    tax = 0
    price = 0
    orderId = 0
    quantity = 0
    productId = 0
    productName = ""

    get amountBeforeTax() {
        return this.price * this.quantity
    }

    get amount() {
        return this.amountBeforeTax + this.amountBeforeTax * (this.tax / 100)
    }

    update() {
        return {
            id: this.id,
            tax: this.tax,
            price: this.price,
            amount: this.amount,
            orderId: this.orderId,
            quantity: this.quantity,
            productId: this.productId,
            productName: this.productName,
            amountBeforeTax: this.amountBeforeTax,
        }
    }

    mapProduct(item: any) {
        this.tax = item.tax
        this.price = item.price
        this.productId = item?.id
        this.productName =  item?.name
        this.quantity = item?.quantity || 1
    }

    assign(item: any) {
        this.id = item.id
        this.tax = item.tax
        this.price = item.price
        this.orderId = item.orderId
        this.quantity = item.quantity
        this.productId = item.productId
        this.productName = item.productName
    }
}
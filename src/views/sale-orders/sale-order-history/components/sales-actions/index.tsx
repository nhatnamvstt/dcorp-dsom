import { Button, Col, DatePicker, Form, InputNumber, Modal, ModalProps, Row, Select } from "antd";
import Title from "antd/es/typography/Title";
import { FC, useEffect, useState } from "react";

import Text from "antd/es/typography/Text";
import SalesSelectTable from "../sales-item-table";
import { formatAmountToNumber, formatNumberToAmount } from "../../../../../utils/number";
import { SaleOrderModel } from "../../model/sale-order.model";
import { companyApis, storeApis } from "../../../../../apis/master-data";
import { SaleOrderStatusString } from "../../../../../types/sale-orders";
import { toast } from "react-toastify";
import { saleOrderApis } from "../../../../../apis/sale-order";

interface SalesActionProps extends ModalProps {
    salesOrder: any;
    onCancel: () => void;
    createComplete: () => void;
}

const SalesAction: FC<SalesActionProps> = (props) => {

    // ** Props
    const { onCancel, createComplete, salesOrder = null, ...modalProps } = props;

    // form
    const [form] = Form.useForm();

    // ** statement
    const [loading, setLoading] = useState<boolean>(false);
    const [companyOptions, setCompanyOptions] = useState<any[]>([]);
    const [storeOptions, setStoreOptions] = useState<any[]>([]);

    const items = Form.useWatch('items', form);
    const discount = Form.useWatch('orderDiscount', form);

    const loadAllOptions = async () => {
        try {
            const [companies, stores]: [any, any] = await Promise.all([
                companyApis.getCompanyWP(),
                storeApis.getStoreWP(),
            ])

            if (companies?.success) {
                const companyRecords = companies?.data?.records || [];
                const companyOps = companyRecords.map((company: any) => ({ value: company.id, label: company.name }));

                setCompanyOptions(companyOps);
            }

            if (stores?.success) {
                const storeRecords = stores?.data?.records || [];
                const storeOps = storeRecords.map((store: any) => ({ value: store.id, label: store.name }));

                setStoreOptions(storeOps);
            }

        } catch (error: any) {
            console.log(error?.message || '')
        }
    }

    const editSaleOrder = async (payload: any) => {
        try {
            setLoading(true);

            const response: any = await saleOrderApis.updateSaleOrder(payload);

            if (response?.success) {
                createComplete();
            }

        } catch(error: any) {
            toast.error(error?.message || '')
        } finally {
            setLoading(false);
        }
    };

    const createSaleOrder = async (payload: any) => {
        try {
            setLoading(true);
            const response: any = await saleOrderApis.createSaleOrder(payload);

            if (response?.success) {
                createComplete();
            }

        } catch(error: any) {
            toast.error(error?.message || '')
        } finally {
            setLoading(false);
        }
    }

    const handleFormSubmit = (formData: any) => {
        if (!formData.items.length) {
            toast.error('Chưa chọn sản phẩm order');
            return;
        }

        const salesOrderModel = new SaleOrderModel();
        salesOrderModel.assign({...formData, id: salesOrder?.id || 0 });

        if (salesOrder) {
            editSaleOrder(salesOrderModel.updatePayload);
            return;
        }

        createSaleOrder(salesOrderModel.createPayload);
    }

    useEffect(() => {
        const formValue = SaleOrderModel.build({
            items,
            orderDiscount: discount,
        }).formValue;

        form.setFieldValue('orderTotal', formValue.orderTotal)
    }, [items, discount]);

    useEffect(() => {
        if (!modalProps?.open) {
            form.resetFields();
        } else {
            if (salesOrder) {
                console.log(SaleOrderModel.build(salesOrder).formValue);
                form.setFieldsValue(SaleOrderModel.build(salesOrder).formValue)
            }
        }
    }, [modalProps?.open, salesOrder])


    useEffect(() => {
        loadAllOptions();
    }, []);

    return (
        <Modal
            width={'100vw'}
            {...modalProps}
            footer={false}
            onCancel={onCancel}
        >
            <Form
                form={form}
                layout="vertical"
                disabled={loading}
                initialValues={SaleOrderModel.build(salesOrder).formValue}
                onFinish={handleFormSubmit}
            >
                <Form.Item
                    name="orderDate"
                    label="Ngày tạo đơn"
                    required
                    rules={[
                        { required: true, message: '' }
                    ]}
                    className="mb-2 w-full"
                >
                    <DatePicker className="w-full" />
                </Form.Item>

                <Row gutter={[16, 16]}>
                    <Col span={12}>
                        <Form.Item
                            name="companyId"
                            label="Khách hàng"
                            required
                            rules={[
                                { required: true, message: '' }
                            ]}
                            className="mb-2"
                        >
                            <Select options={companyOptions} />
                        </Form.Item>
                    </Col>

                    <Col span={12}>
                        <Form.Item
                            name="storeId"
                            label="Nhà hàng"
                            required
                            rules={[
                                { required: true, message: '' }
                            ]}
                            className="mb-2"
                        >
                            <Select options={storeOptions} />
                        </Form.Item>
                    </Col>
                </Row>

                <Form.Item
                    name="items"
                    label=""
                >
                    <SalesSelectTable />
                </Form.Item>

                <Row gutter={[16, 16]}>
                    <Col span={12}>
                        <Form.Item
                            name="orderDiscount"
                            label="Giảm giá"
                            rules={[
                                {
                                    required: true,
                                    message: ''
                                }
                            ]}
                        >
                            <InputNumber className="w-full" suffix="%" />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item
                            name="orderStatus"
                            label="Trạng thái"
                            rules={[
                                {
                                    required: true,
                                    message: ''
                                }
                            ]}
                        >
                            <Select options={Object.entries(SaleOrderStatusString).map((entry: any) => ({
                                value: entry[0],
                                label: entry[1],
                            }))} />
                        </Form.Item>
                    </Col>
                </Row>

                <Form.Item
                    name="orderTotal"
                    label="Tổng đơn giá"
                >
                    <InputNumber
                        readOnly
                        className="w-full"
                        formatter={(value: number | undefined) => formatNumberToAmount(value as number)}
                        parser={(value: string | undefined) => formatAmountToNumber(value as string)}
                    />
                </Form.Item>

                <Row gutter={[12, 12]} align={'middle'} justify={'end'}>
                    <Col>
                        <Button htmlType="button" danger onClick={onCancel}>Hủy</Button>
                    </Col>
                    <Col>
                        <Button className="bg-primary" htmlType="submit" type="primary" loading={loading}>Lưu</Button>
                    </Col>
                </Row>
            </Form>
        </Modal>
    )
}


export default SalesAction;
import { Button, Col, InputNumber, Row, Table, TableProps } from "antd";
import { formatNumberToAmount, formatNumberToPercent } from "../../../../../utils/number";

import {
    CloseOutlined
} from '@ant-design/icons';
import { FC, useEffect, useState } from "react";
import ItemSelect from "../item-select";
import { ItemSelectModel } from "../../model/selected.model";

interface SalesSelectTableProps {
    value?: any[];
    onChange?: (value: any[]) => void;
}

const SalesSelectTable: FC<SalesSelectTableProps> = (props) => {

    // ** Props
    const { value = [], onChange } = props;

    // ** statements
    const [isSelectedModalOpen, setIsSelectedModalOpen] = useState<boolean>(false);

    const selectItem = (item: any) => {
        const selects = item.map((product: any) => {
            const selectedModel = new ItemSelectModel()
            selectedModel.mapProduct(product)
            return selectedModel.update();
        })

        onChange?.([...value, ...selects])
        setIsSelectedModalOpen(false);
    }

    const handleChangeQuantity = (index: number, quantity: number) => {
        const updateValue = value;
        const findUpdateItem = updateValue[index]
        
        const selectedModel = new ItemSelectModel()
        selectedModel.assign(findUpdateItem)
        selectedModel.quantity = quantity
        
        const updateItem = selectedModel.update();

        updateValue[index] = updateItem;

        onChange?.([...updateValue])
    };

    const removeItem = (item: any) => {
        const filteredItem = value.filter((product: any) => item.productId !== product.productId)
        onChange?.([...filteredItem]);
    }

    const columns: TableProps<any>['columns'] = [
        {
            title: 'Stt',
            key: 'no',
            dataIndex: 'no',
            className: 'text-center',
            render: (text: number, record: any, index: number) => index + 1
        },

        {
            title: 'Tên',
            key: 'productName',
            dataIndex: 'productName',
        },

        {
            title: 'Đơn giá',
            key: 'price',
            dataIndex: 'price',
            className: 'text-right',
            render: (amount: number) => formatNumberToAmount(amount)
        },

        {
            title: 'Số lượng',
            key: 'quantity',
            dataIndex: 'quantity',
            className: 'text-right',
            render: (amount: number, record: any, index: number) => (
                <InputNumber 
                    value={amount}
                    className="w-12"
                    onChange={(value: any) => handleChangeQuantity(index, value)}
                />
            )
        },

        {
            title: 'Thuế',
            key: 'tax',
            dataIndex: 'tax',
            className: 'text-right',
            render: (amount: number) => formatNumberToPercent(amount)
        },

        {
            title: 'Tổng giá',
            key: 'amount',
            dataIndex: 'amount',
            className: 'text-right',
            render: (amount: number) => formatNumberToAmount(amount)
        },

        {
            title: '',
            key: 'action',
            dataIndex: 'action',
            className: 'text-right',
            render: (text: any, record: any) => (
                <div className="flex items-center space-x-2 justify-end">
                    <div className="border rounded p-1 cursor-pointer" onClick={() => removeItem(record)}>
                        <CloseOutlined />
                    </div>
                </div>
            )
        },
    ]

    return (
        <>
            <Row gutter={[8, 8]}>
                <Col span={24}>
                    <Row justify={'space-between'} align={'middle'}>
                        <p className="font-bold">
                            Chọn sản phẩm
                        </p>

                        <Button type="primary" className="bg-primary" onClick={() => setIsSelectedModalOpen(true)}>
                            Thêm
                        </Button>
                    </Row>
                </Col>

                <Col span={24}>
                    <Table
                        columns={columns}
                        dataSource={value}
                        locale={{emptyText: 'Chưa có sản phẩm nào được chọn'}}
                    />
                </Col>
            </Row>

            {isSelectedModalOpen && (
                <ItemSelect
                    open={isSelectedModalOpen}
                    onSelect={selectItem}
                    onCancel={() => setIsSelectedModalOpen(false)}
                />
            )}
        </>
    )
};

export default SalesSelectTable;
import { Button, Card, Col, Modal, ModalProps, Pagination, Row } from "antd";
import { FC, useEffect, useMemo, useState } from "react";
import { useProductCategoryContext } from "../../../../master-data/products/context";
import { IProductCategory } from "../../../../../types/master-data/product-categories";
import Title from "antd/es/typography/Title";

// ** Data
import Products from '../../../../.../../../@fake-data/master-data/products.json';
import { ProductType } from "../../../../../types/master-data/products";
import usePagination from "../../../../../utils/hooks/use-pagination";
import classNames from "classnames";
import { toast } from "react-toastify";
import { DEFAULT_LIMIT } from "../../../../../constant";
import { productApis } from "../../../../../apis/master-data";

interface ItemSelectProp extends ModalProps {
    selectedValue?: any[];

    onCancel?: () => void;
    onSelect?: (items: any[]) => void;
}

const ItemSelect: FC<ItemSelectProp> = (props) => {
    // ** props
    const { selectedValue, onCancel, onSelect } = props;

    // ** pagination
    const { setTotal, pagination, page } = usePagination();

    // ** statement
    const [selected, setSelected] = useState<any[]>([]);
    const [products, setProduct] = useState<any[]>([]);
    const [selectedCategory, setSelectedCategory] = useState<number | null>(null);

    // ** product categories
    const { categories } = useProductCategoryContext();

    const selectedProductIds = useMemo(() => {
        return selected.map((item: any) => item.id)
    }, [selected]);

    const handleProductClick = (item: any) => {
        if (!selectedProductIds.includes(item.id)) {
            setSelected([...selected, item]);
            return;
        }

        const filterProducts = selected.filter((item: any) => !selectedProductIds.includes(item.id));
        setSelected(filterProducts);
    }

    const loadProducts = async () => {
        try {
            const response = await productApis.getProducts({
                pageIndex: page,
                pageSize: DEFAULT_LIMIT,
                productCategoryId: selectedCategory,
            });

            setTotal(response?.data?.total);
            setProduct(response?.data?.records);

        } catch (error: any) {
            toast.error(error.message);
        }
    };

    const handleConfirmButtonClick = () => {
        if (selected.length) {
            onSelect?.(selected);
            return;
        } 
        toast.error('Chọn mặt hàng cần tạo đơn')
    };

    useEffect(() => {
        if (selectedCategory) {
            loadProducts();
        }
    }, [selectedCategory]);

    useEffect(() => {
        if (categories && !selectedCategory) {
            setSelectedCategory(categories?.[0]?.id || null);
        }
    }, [categories, selectedCategory]);

    useEffect(() => {
        if (selectedValue?.length) {
            setSelected(selectedValue);
            setSelectedCategory(selectedValue?.[0]?.productCategoryId || null);
        }
    }, [selectedValue])

    return (
        <Modal width="800px" open={props.open} title="Chọn sản phẩm" footer={false} onCancel={onCancel}>
            <Row gutter={[24, 12]}>
                <Col span={6}>
                    <Row>
                        <Col span={24}>
                            <Title level={5}>Nhóm hàng</Title>
                        </Col>

                        <Col span={24}>
                            <Row gutter={[12, 4]}>
                                {
                                    categories.map((item: IProductCategory) => (
                                        <Card className={classNames(["[&>.ant-card-body]:p-2 w-full cursor-pointer hover:bg-primary hover:text-[white]"], {
                                            'bg-primary text-[white]': selectedCategory === item.id
                                        })}
                                            onClick={() => setSelectedCategory(item.id)}
                                        >
                                            {item.name}
                                        </Card>
                                    ))
                                }
                            </Row>
                        </Col>
                    </Row>
                </Col>

                <Col span={18}>
                    <Row>
                        <Col span={24}>
                            <Title level={5}>Hàng sản phẩm</Title>
                        </Col>

                        <Col span={24} className="w-full flex flex-col space-y-2">
                            <Row gutter={[12, 4]}>
                                {
                                    products.map((item: ProductType) => (
                                        <Card className={classNames([
                                            "[&>.ant-card-body]:p-2 w-full cursor-pointer hover:bg-primary hover:text-[white]",
                                            {
                                                "bg-primary text-[white]": selectedProductIds.includes(item.id)
                                            }
                                        ])}
                                            onClick={() => handleProductClick(item)}
                                        >
                                            {item.name}
                                        </Card>
                                    ))
                                }
                            </Row>
                            <div className="flex items-center justify-center">
                                <Pagination {...pagination} />
                            </div>
                        </Col>
                    </Row>
                </Col>

                <Col span={24} className="w-full flex items-center justify-end space-x-2">
                    <Button htmlType="button" danger onClick={onCancel}>Hủy</Button>
                    <Button htmlType="submit" type="primary" className="bg-primary" onClick={handleConfirmButtonClick}>Xác nhận</Button>
                </Col>
            </Row>
        </Modal>
    )
}

export default ItemSelect;
import Title from "antd/es/typography/Title";
import { FC } from "react";

const WelcomePage: FC = () => {
    return (
        <div className="flex flex-col items-center h-full justify-center">
            <Title className="mb-0" level={2}>Chào mừng</Title>
            <Title level={1}>Trang quản trị DSOM</Title>
        </div>
    )
}

export default WelcomePage;
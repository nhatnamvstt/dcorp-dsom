import { FC } from "react";

const NotFoundPage: FC = () => {
    return (
        <div style={{ height: '100vh', width: '100vw'}}>
            404 Not Found
        </div>
    )
};

export default NotFoundPage;
import { useEffect, useMemo, useState } from "react";
import PageHeader from "../../../components/containers/page-header";
import usePagination from "../../../utils/hooks/use-pagination";
import { formatTimeToString } from "../../../utils/time";
import { Card, Col, DatePicker, Row, Table, TableProps } from "antd";
import Title from "antd/es/typography/Title";
import Text from "antd/es/typography/Text";
import { formatNumberToAmount } from "../../../utils/number";
import { SaleOrderStatusEnum, SaleOrderStatusString } from "../../../types/sale-orders";
import { toast } from "react-toastify";
import { DATE_FORMAT, DEFAULT_LIMIT, WITHOUT_LIMIT } from "../../../constant";
import dayjs, { Dayjs } from "dayjs";
import { reportApis } from "../../../apis/reports";
import { saleOrderApis } from "../../../apis/sale-order";

const SaleOrderReportByDatePage = () => {
    // ** hooks
    const { total, setTotal, pagination, page } = usePagination();

    // ** statement
    const [dateRange, setDateRange] = useState<[Dayjs, Dayjs]>([dayjs().startOf('month'), dayjs()]);
    const [reports, setReports] = useState<any>(null);
    const [salesOrders, setSalesOrders] = useState<any[]>([]);
    const [loading, setLoading] = useState<boolean>(false);

    const dateRangeString = useMemo(() => {
        const startDate = dayjs(dateRange[0]).format(DATE_FORMAT.YMD)
        const endDate = dayjs(dateRange[1]).format(DATE_FORMAT.YMD)

        return {
            startDate,
            endDate,
        }
    }, [dateRange]);

    const totalAmount = useMemo(() => {
        return salesOrders.reduce((total: number, order: any) => {
            const amount = order?.orderTotal || 0
            return total + amount;
        }, 0);
    }, [salesOrders])

    const columns: TableProps<any>['columns'] = [
        {
            title: 'Stt',
            key: 'no',
            dataIndex: 'no',
            className: 'text-center',
            render: (text: number, record: any, index: number) => index + (page - 1) * DEFAULT_LIMIT + 1
        },

        {
            title: 'Ngày tạo',
            key: 'createdDate',
            dataIndex: 'createdDate',
            render: (date: string) => formatTimeToString(date)
        },

        {
            title: 'Số đơn',
            key: 'orderNumber',
            dataIndex: 'orderNumber',
        },

        {
            title: 'Khách hàng',
            key: 'companyName',
            dataIndex: 'companyName',
        },

        {
            title: 'Trạng thái',
            key: 'orderStatus',
            dataIndex: 'orderStatus',
            render: (status: number) => SaleOrderStatusString[status as SaleOrderStatusEnum]
        },

        {
            title: 'Tổng tiền',
            key: 'orderTotal',
            className: "text-right",
            dataIndex: 'orderTotal',
            render: (amount: number) => formatNumberToAmount(amount)
        },
    ]

    const loadReports = async () => {
        try {
            setLoading(true);

            const [statistics, saleOrderByDates]: any[] = await Promise.all([
                reportApis.getReportByDate({
                    status: 0,
                    pageIndex: page,
                    pageSize: WITHOUT_LIMIT,
                    startDate: dateRangeString.startDate,
                    endDate: dateRangeString.endDate,
                }),
                saleOrderApis.getSaleOrders({
                    status: 0,
                    pageIndex: page,
                    pageSize: DEFAULT_LIMIT,
                    startDate: dateRangeString.startDate,
                    endDate: dateRangeString.endDate,
                })
            ]);

            if (statistics?.success) {
                setReports(statistics?.data || {});
            }

            if (saleOrderByDates?.success) {
                setSalesOrders(saleOrderByDates?.data?.records || []);
                setTotal(saleOrderByDates?.data?.total || 0)
            }

        } catch (error: any) {
            toast.error(error.message);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        loadReports();
    }, [dateRange, page])

    return (
        <PageHeader
            title="Danh sách hóa đơn theo ngày"
            breadcrumbs={[{ title: "Danh sách hóa đơn theo ngày" }]}
        >
            <Row className="space-y-8">
                <Col span={24}>
                    <Row gutter={[16, 16]}>
                        <Col span={6}>
                            <Card className="[&>.ant-card-body]:p-4">
                                <Title level={5} className="text-[14px]">Tổng đơn hàng</Title>
                                <Title level={5} className="my-2">{formatNumberToAmount(total)}</Title>
                            </Card>
                        </Col>

                        <Col span={6}>
                            <Card className="[&>.ant-card-body]:p-4">
                                <Title level={5} className="text-[14px]">Tổng giá trị</Title>
                                <Title level={5} className="my-2">{formatNumberToAmount(totalAmount)}</Title>
                            </Card>
                        </Col>

                        <Col span={6}>
                            <Card className="[&>.ant-card-body]:p-4">
                                <Title level={5} className="text-[14px]">Đơn thành công</Title>
                                <Title level={5} className="my-2">{formatNumberToAmount(reports?.total_Completed || 0)}</Title>
                            </Card>
                        </Col>

                        <Col span={6}>
                            <Card className="[&>.ant-card-body]:p-4">
                                <Title level={5} className="text-[14px]">Đơn hủy</Title>
                                <Title level={5} className="my-2">{formatNumberToAmount(reports?.total_Cancelled || 0)}</Title>
                            </Card>
                        </Col>
                    </Row>
                </Col>

                <Col span={24}>
                    <Row gutter={[12, 12]}>
                        <Col span={24}>
                            <Row align={'bottom'} justify={'space-between'}>
                                <div className="relative">
                                    <Text className="absolute -top-5 left-2 !z-[999] bg-white p-1 pt-2.5 pb-0 text-[10px]">Ngày</Text>

                                    <DatePicker.RangePicker value={dateRange} onChange={(dateRange: [any, any]) => setDateRange(dateRange)} />
                                </div>
                            </Row>
                        </Col>

                        <Col span={24}>
                            <Table
                                rowKey={'id'}
                                loading={loading}
                                columns={columns}
                                dataSource={salesOrders}
                                pagination={pagination}
                                locale={{ emptyText: 'Không có dữ liệu báo cáo' }}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </PageHeader>
    )
}

export default SaleOrderReportByDatePage;    
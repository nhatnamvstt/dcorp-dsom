import { FC, LazyExoticComponent, lazy } from "react";

import routePaths from "../route-paths";

const SalesOrderHistoryPage = lazy(() => import("../../views/sale-orders/sale-order-history")); 

export const saleOrderRoute: {href: string, component: LazyExoticComponent<FC<{}>>}[] = [
    {
        href: routePaths.salesOrders,
        component: SalesOrderHistoryPage,
    },
]
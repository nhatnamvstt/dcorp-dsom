import { FC, LazyExoticComponent, lazy } from "react";

import routePaths from "../route-paths";

const SaleOrderReportByDatePage = lazy(() => import("../../views/reports/sales-order-by-date"));

export const reportRoutes: {href: string, component: LazyExoticComponent<FC<{}>>}[] = [
    {
        href: routePaths.salesOrderReportByDate,
        component: SaleOrderReportByDatePage,
    }
]
import routePaths from "./route-paths";
import { FC, LazyExoticComponent, lazy } from "react"

import { reportRoutes } from "./reports";
import { saleOrderRoute } from "./sale-order";
import { masterDataRoutes } from "./master-data";

const WelcomePage = lazy(() => import("../views/welcome")); 

const pageRoutes: {href: string, component: LazyExoticComponent<FC<{}>>}[] = [
    {
        href: routePaths.welcome,
        component: WelcomePage,
    },

    ...masterDataRoutes,
    
    ...saleOrderRoute,

    ...reportRoutes,
]

export default pageRoutes
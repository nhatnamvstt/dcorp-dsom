const routePaths = {
    // ** main page
    welcome: '/',

    // ** master data
    products: '/products',
    partners: '/partners',
    restaurants: '/restaurants',
    productCategories: '/product-categories',

    // ** so
    salesOrders: '/sales-orders',
    salesOrderReportByDate: '/sales-order-report',
}; 

export default routePaths;

import { FC, LazyExoticComponent, lazy } from "react";
import routePaths from "../route-paths";

const PartnerPage = lazy(() => import("../../views/master-data/partner"));
const ProductPage = lazy(() => import("../../views/master-data/products"));
const RestaurantPage = lazy(() => import("../../views/master-data/restaurant"));
const ProductCategoryPage = lazy(() => import("../../views/master-data/product-category"));


export const masterDataRoutes: {href: string, component: LazyExoticComponent<FC<{}>>}[] = [
    {
        href: routePaths.products,
        component: ProductPage,
    },

    {
        href: routePaths.productCategories,
        component: ProductCategoryPage,
    },

    {
        href: routePaths.partners,
        component: PartnerPage,
    },

    {
        href: routePaths.restaurants,
        component: RestaurantPage,
    },
]
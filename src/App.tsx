import { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';

import Layout from './components/layouts';

import NotFoundPage from './views/not-found';

import pageRoutes from './routes';

// ** styles
import './App.css';

function App() {
  return (
    <Routes>
      <Route element={<Layout />}>
        {
          pageRoutes.map(({ component: Component, href }) => (
            <Route 
              key={href}
              path={href}
              element={
                <Suspense>
                  <Component />
                </Suspense>
              }
            />
          ))
        }
      </Route>

      <Route element={<Layout />}>
        <Route 
          path="*"
          element={<NotFoundPage />}
        />
      </Route>
    </Routes>
  );
}

export default App;

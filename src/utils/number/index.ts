export const formatNumberToAmount = (amount: number) => {
    return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export const formatNumberToPercent = (amount: number) => {
    return (amount).toFixed(0) + '%';
}

export const formatAmountToNumber = (amount: string) => {
    return +amount!.replace(/\$\s?|(,*)/g, '');
}
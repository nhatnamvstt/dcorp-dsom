import dayjs from "dayjs"

export const formatTimeToString = (date: string = new Date().toString(), format: string = 'DD/MM/YYYY') => {
    return dayjs(date).format(format);
}
import { Modal } from "antd"

const { confirm } = Modal

export const ConfirmAlert = (confirmConfig: any) => {
    confirm({
        ...confirmConfig,
        okButtonProps: {
            className: 'bg-primary'
        }
    })
}
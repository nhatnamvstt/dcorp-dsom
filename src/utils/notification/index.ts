import { notification } from "antd";

const [api] = notification.useNotification();

type NotificationType = 'success' | 'info' | 'warning' | 'error';

export const openNotification = (type: NotificationType, message: string, description: string)  => {
    api[type]({
        message: message,
        description: description,
        placement: 'topLeft'
    })
}
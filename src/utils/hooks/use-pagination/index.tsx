import { useState } from "react";

export const DEFAULT_PAGE_SIZE = 10;

const usePagination = () => {
    const [page, setPage] = useState<number>(1);
    const [total, setTotal] = useState<number>(0);

    const handleChangePage = (page: number) => {
        setPage(page);
    };

    return {
        page,
        total,
        
        pagination: {
            current: page,
            total: total,
            pageSize: DEFAULT_PAGE_SIZE,
            onChange: handleChangePage,
        },

        setPage,
        setTotal,
        handleChangePage,
    }
};

export default usePagination;
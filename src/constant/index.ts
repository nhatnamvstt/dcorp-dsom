export const DEFAULT_LIMIT = 10;

export const WITHOUT_LIMIT = 500;

export const DATE_FORMAT = {
    YMD: 'YYYY-MM-DD',
}
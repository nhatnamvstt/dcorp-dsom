export type ProductType = {
    id?: number;
    tax?: number;
    name?: string;
    code?: string;
    price?: number;
    status?: number;
    deleted?: boolean;
    description?: string;
    productCategoryId?: number;
}

export type ProductActionPayloadType = {
    tax: number;
    name: string;
    code: string;
    price: number;
    status: number;
    description: string;
    productCategoryId: number;
}
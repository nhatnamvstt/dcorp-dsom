export interface IProductCategory {
    id: number;
    name: string;
    code: string;
    deleted: boolean;
    description: string;
}

export interface IProductCategoriesResponse extends IProductCategory {
    children: IProductCategory[],
}
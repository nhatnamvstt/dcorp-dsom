export enum SaleOrderStatusEnum {
    Pending = 0,
    Accepted = 1,
    InProcess = 2,
    Completed = 3,
    Cancelled = 4,
}

export const SaleOrderStatusString = {
    [SaleOrderStatusEnum.Pending]: 'Đang chờ',
    [SaleOrderStatusEnum.Accepted]: 'Xác nhận',
    [SaleOrderStatusEnum.InProcess]: 'Tiến hành',
    [SaleOrderStatusEnum.Completed]: 'Thành công',
    [SaleOrderStatusEnum.Cancelled]: 'Hủy',
}
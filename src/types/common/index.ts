export type OptionType<T> = {
    value: T,
    label: string,
}

// ** status
export enum StatusEnum {
    Draft = 1,
    Deactivate = 2,
    Active = 3,
}

export const StatusLabelEnum = {
    [StatusEnum.Draft]: 'Nháp',
    [StatusEnum.Deactivate]: 'Chưa kích hoạt',
    [StatusEnum.Active]: 'Kích hoạt'
}